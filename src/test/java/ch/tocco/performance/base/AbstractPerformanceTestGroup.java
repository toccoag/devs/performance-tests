package ch.tocco.performance.base;

import java.net.ConnectException;
import java.util.*;

import ch.tocco.performance.base.beans.PrimaryKeyListBean;
import ch.tocco.performance.base.beans.RelationDefinitionBean;
import ch.tocco.performance.base.beans.TestDataGenerationRequestBean;
import ch.tocco.performance.base.connectors.DatabaseConnector;
import ch.tocco.performance.base.connectors.OpenshiftConnector;
import ch.tocco.performance.base.managers.ResultManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
public abstract class AbstractPerformanceTestGroup {

    private final Logger logger = LoggerFactory.getLogger(AbstractPerformanceTestGroup.class);

    @Value("${performance.test.baseUrl}")
    protected String baseUrl;

    @Value("${performance.test.user}")
    protected String restUsername;

    @Value("${performance.test.apiKey}")
    protected String restApiKey;

    @Value("${performance.test.warmupIterations}")
    protected int warmupIterations;

    @Value("${performance.test.measurementIterations}")
    protected int measurementIterations;

    @Value("${performance.startup.retries}")
    private int retries;

    @Autowired
    private DatabaseConnector databaseConnector;

    @Autowired
    private OpenshiftConnector openshiftConnector;

    @Autowired
    private ResultManager resultManager;

    @BeforeAll
    public final void initializeTestInstance() throws Exception {
        shutdownTestInstance();
        databaseConnector.copyDatabase();
        openshiftConnector.start();

        WebClient webClient = buildDefaultWebClient().build();
        waitUntilApiIsReady(webClient);
        configureTestInstance(webClient);
        databaseConnector.updateStatistics();
    }

    protected WebClient.Builder buildDefaultWebClient() {
        return WebClient.builder()
            .baseUrl(baseUrl)
            .defaultHeader("X-Business-Unit", "test1")
            .defaultHeaders(httpHeaders -> httpHeaders.setBasicAuth(restUsername, restApiKey));
    }

    @AfterAll
    public final void shutdownTestInstance() {
        openshiftConnector.terminate();
        databaseConnector.dropDatabase();
    }

    private void waitUntilApiIsReady(WebClient webClient) throws Exception {
        for (int i = 0; i < retries; i++) {
            ClientResponse response = webClient.get()
                    .uri("/")
                    .exchange()
                    .onErrorResume(e -> Mono.empty())
                    .block();

            if (response != null && response.statusCode().is2xxSuccessful()) {
                logger.info(String.format("Nice is ready after %s seconds", i));
                return;
            }
            Thread.sleep(1000);
        }
        throw new ConnectException(String.format("Giving up waiting until nice is ready after %s seconds", retries));
    }

    protected void configureTestInstance(WebClient webClient) {
    }

    protected int getWarmupIterations() {
        return warmupIterations;
    }

    protected int getMeasurementIterations() {
        return measurementIterations;
    }

    protected List<String> insertTestData(WebClient webClient, String model, int count, Map<String, RelationDefinitionBean> relations) {
        return insertTestData(webClient, model, count, relations, Map.of());
    }

    protected List<String> insertTestData(WebClient webClient, String model, int count, Map<String, RelationDefinitionBean> relations, Map<String, Object> fields) {
        ClientResponse response = webClient.post()
                .uri("/test-data/entity")
                .header("X-Business-Unit", "test1")
                .bodyValue(new TestDataGenerationRequestBean(model, count, relations, fields))

                .exchange()
                .block();

        assertNotNull(response);
        assertTrue(response.statusCode().is2xxSuccessful());
        return response.bodyToMono(PrimaryKeyListBean.class).block().getValues();
    }

    @SuppressWarnings("unused")
    protected abstract class AbstractPerformanceTestCase {

        protected String name = getClass().getName();

        @Test
        public final void runTest() {
            try {
                UUID requestId = UUID.randomUUID();
                assertNotNull(requestId);

                WebClient webClientWarmup = buildDefaultWebClient()
                    .build();

                for (int i = 0; i < getWarmupIterations(); i++) {
                    executeWarmup(webClientWarmup);
                }

                WebClient webClientMeasurement = buildDefaultWebClient()
                    .defaultHeader("X-Request-Id", requestId.toString())
                    .build();

                for (int i = 0; i < getMeasurementIterations(); i++) {
                    executeTest(webClientMeasurement);
                }

                resultManager.process(requestId, name, buildDefaultWebClient().build());
            } catch (Exception e) {
                resultManager.testFailed(name);
            }
        }

        protected abstract void executeTest(WebClient webClient) throws Exception;

        protected void executeWarmup(WebClient webClient) throws Exception {
            executeTest(webClient);
        }
    }
}
