package ch.tocco.performance.base.connectors;

import ch.tocco.performance.base.beans.TestExecutionBean;
import ch.tocco.performance.base.beans.TestExecutionEvaluationBean;

import java.util.List;

public interface DatabaseConnector {

    void dropDatabase();

    void copyDatabase();

    void createExecution(int buildId, String revision);

    void createTestExecution(TestExecutionBean bean);

    List<TestExecutionEvaluationBean> getTestExecutions(String test, int limit);

    void updateStatistics();
}
