package ch.tocco.performance.base.connectors;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevOpenshiftConnector implements OpenshiftConnector {

    @Override
    public void start() {

    }

    @Override
    public void terminate() {

    }
}
