package ch.tocco.performance.base.connectors;

import ch.tocco.performance.base.beans.TestExecutionBean;
import ch.tocco.performance.base.beans.TestExecutionEvaluationBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@Profile("!dev")
public class ProductionDatabaseConnector implements DatabaseConnector {
    private final Logger logger = LoggerFactory.getLogger(ProductionDatabaseConnector.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${performance.database.database}")
    private String database;

    @Value("${performance.database.databaseTemplate}")
    private String databaseTemplate;

    @Override
    public void dropDatabase() {
        logWithExecutionTime(() -> {
            closeConnections(database);
            jdbcTemplate.execute("DROP DATABASE IF EXISTS " + database);
        }, "Database " + database + " dropped");
    }

    @Override
    public void copyDatabase() {
        logWithExecutionTime(() -> {
            closeConnections(databaseTemplate);
            jdbcTemplate.execute("CREATE DATABASE " + database + " WITH TEMPLATE " + databaseTemplate);
        }, "Database " + database + " created from template " + databaseTemplate);
    }

    private void closeConnections(String database) {
        jdbcTemplate.execute(" SELECT pg_terminate_backend(pg_stat_activity.pid) " +
                "FROM pg_stat_activity WHERE pg_stat_activity.datname = '" + database + "'");
    }

    @Override
    public void createExecution(int buildId, String revision) {
        jdbcTemplate.update("INSERT INTO execution (build_id, revision, created) VALUES(?, ?, ?) " +
                "ON CONFLICT DO NOTHING", buildId, revision, new Timestamp((new Date()).getTime()));
    }

    @Override
    public void createTestExecution(TestExecutionBean bean) {
        jdbcTemplate.update("INSERT INTO test_execution (test, build_id, warmup_iterations, measurement_iterations, " +
                        "mean, max, min, raw) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", bean.getTest(), bean.getBuildId(),
                bean.getWarmupIterations(), bean.getMeasurementIterations(), bean.getMean(), bean.getMax(),
                bean.getMin(), bean.getRaw());
    }

    @Override
    public List<TestExecutionEvaluationBean> getTestExecutions(String test, int limit) {
        List<TestExecutionEvaluationBean> list = jdbcTemplate.query("SELECT e.build_id, e.created, te.mean, te.max, te.min " +
                "FROM test_execution AS te " +
                "LEFT JOIN execution AS e ON e.build_id = te.build_id " +
                "WHERE te.test = ? " +
                "ORDER BY e.build_id DESC " +
                "LIMIT ?", new Object[]{test, limit}, new TestExecutionEvaluationMapper());
        Collections.reverse(list);
        return list;
    }

    @Override
    public void updateStatistics() {
        logWithExecutionTime(() -> jdbcTemplate.update("ANALYZE"), "Database statistics updated");
    }

    private void logWithExecutionTime(ExecutionTimeOperation op, String message) {
        long startTime = System.currentTimeMillis();
        op.execute();
        long endTime = System.currentTimeMillis();
        logger.info(message + " [" + (endTime - startTime) + " ms]");
    }

    private interface ExecutionTimeOperation {
        void execute();
    }

    private static final class TestExecutionEvaluationMapper implements RowMapper<TestExecutionEvaluationBean> {
        public TestExecutionEvaluationBean mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new TestExecutionEvaluationBean(rs.getInt("build_id"), rs.getTimestamp("created"),
                    rs.getDouble("mean"), rs.getInt("max"), rs.getInt("min"));
        }
    }
}
