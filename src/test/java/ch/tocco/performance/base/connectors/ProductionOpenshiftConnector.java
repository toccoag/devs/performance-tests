package ch.tocco.performance.base.connectors;

import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.openshift.client.DefaultOpenShiftClient;
import io.fabric8.openshift.client.OpenShiftClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Profile("!dev")
public class ProductionOpenshiftConnector implements OpenshiftConnector {
    private final String deploymentConfigName = "nice";
    private final OpenShiftClient client;
    private final Logger logger = LoggerFactory.getLogger(ProductionOpenshiftConnector.class);

    @Autowired
    private Environment environment;

    public ProductionOpenshiftConnector(@Value("${performance.openshift.url}") String url,
                                        @Value("${performance.openshift.oauth}") String oauth,
                                        @Value("${performance.openshift.project}") String project) {
        Config config = new ConfigBuilder()
                .withMasterUrl(url)
                .withOauthToken(oauth)
                .withNamespace(project)
                .build();
        client = new DefaultOpenShiftClient(config);

        try {
            client.projects().list();
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Login failed");
        }
    }

    @Override
    public void start() {
        scale(1);
        logger.info("Container started");
    }

    @Override
    public void terminate() {
        scale(0);
        logger.info("Container terminated");
    }

    private void scale(int count) {
        client.deploymentConfigs().withName(deploymentConfigName).scale(count);
    }
}
