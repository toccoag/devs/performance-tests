package ch.tocco.performance.base.connectors;

import ch.tocco.performance.base.beans.TestExecutionBean;
import ch.tocco.performance.base.beans.TestExecutionEvaluationBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile("dev")
public class DevDatabaseConnector implements DatabaseConnector {

    @Override
    public void dropDatabase() {

    }

    @Override
    public void copyDatabase() {

    }

    @Override
    public void createExecution(int buildId, String revision) {

    }

    @Override
    public void createTestExecution(TestExecutionBean bean) {

    }

    @Override
    public List<TestExecutionEvaluationBean> getTestExecutions(String test, int limit) {
        return List.of();
    }

    @Override
    public void updateStatistics() {

    }
}
