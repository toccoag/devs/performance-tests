package ch.tocco.performance.base.connectors;

public interface OpenshiftConnector {

    void start();

    void terminate();
}
