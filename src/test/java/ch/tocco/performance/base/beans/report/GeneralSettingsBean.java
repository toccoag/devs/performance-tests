package ch.tocco.performance.base.beans.report;

public class GeneralSettingsBean {
    private String language;
    private String filename;
    private String corporateDesign;
    private String archiveType;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getCorporateDesign() {
        return corporateDesign;
    }

    public void setCorporateDesign(String corporateDesign) {
        this.corporateDesign = corporateDesign;
    }

    public String getArchiveType() {
        return archiveType;
    }

    public void setArchiveType(String archiveType) {
        this.archiveType = archiveType;
    }
}
