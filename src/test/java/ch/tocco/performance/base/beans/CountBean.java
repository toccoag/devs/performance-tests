package ch.tocco.performance.base.beans;

public class CountBean {

    private long count;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
