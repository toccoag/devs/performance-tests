package ch.tocco.performance.base.beans;

import java.util.List;

public class EntitiesBean {

    private List<EntityBean> data;

    public List<EntityBean> getData() {
        return data;
    }

    public void setData(List<EntityBean> data) {
        this.data = data;
    }
}
