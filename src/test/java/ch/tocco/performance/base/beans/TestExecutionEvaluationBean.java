package ch.tocco.performance.base.beans;

import java.sql.Timestamp;

public class TestExecutionEvaluationBean {

    private final int buildId;
    private final Timestamp created;
    private final double mean;
    private final int max;
    private final int min;

    public TestExecutionEvaluationBean(int buildId, Timestamp created, double mean, int max, int min) {
        this.buildId = buildId;
        this.created = created;
        this.mean = mean;
        this.max = max;
        this.min = min;
    }

    public int getBuildId() {
        return buildId;
    }

    public Timestamp getCreated() {
        return created;
    }

    public double getMean() {
        return mean;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }
}
