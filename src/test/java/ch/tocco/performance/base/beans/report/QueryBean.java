package ch.tocco.performance.base.beans.report;

public class QueryBean {
    private String where;

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }
}
