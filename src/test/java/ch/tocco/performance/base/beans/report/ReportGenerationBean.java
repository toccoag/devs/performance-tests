package ch.tocco.performance.base.beans.report;

public class ReportGenerationBean {
    private String entityModel;
    private SelectionBean selection;
    private GeneralSettingsBean generalSettings;

    public String getEntityModel() {
        return entityModel;
    }

    public void setEntityModel(String entityModel) {
        this.entityModel = entityModel;
    }

    public SelectionBean getSelection() {
        return selection;
    }

    public void setSelection(SelectionBean selection) {
        this.selection = selection;
    }

    public GeneralSettingsBean getGeneralSettings() {
        return generalSettings;
    }

    public void setGeneralSettings(GeneralSettingsBean generalSettings) {
        this.generalSettings = generalSettings;
    }
}