package ch.tocco.performance.base.beans;

import java.util.Map;

public class RelationDefinitionBean {

    private final GenerationType generationType;
    private final int count;
    private final Map<String, RelationDefinitionBean> relationData;
    private final Map<String, Object> fieldData;

    public static RelationDefinitionBean anyEntity() {
        return new RelationDefinitionBean(GenerationType.FIRST, 0, Map.of(), Map.of());
    }

    public static RelationDefinitionBean random(int count) {
        return new RelationDefinitionBean(GenerationType.RANDOM, count, Map.of(), Map.of());
    }

    public static RelationDefinitionBean nested(Map<String, RelationDefinitionBean> relationData) {
        return nested(relationData, Map.of());
    }

    public static RelationDefinitionBean nested(Map<String, RelationDefinitionBean> relationData, Map<String, Object> fieldData) {
        return new RelationDefinitionBean(GenerationType.NESTED, 1, relationData, fieldData);
    }

    public static RelationDefinitionBean nestedMany(int count, Map<String, RelationDefinitionBean> relationData, Map<String, Object> fieldData) {
        return new RelationDefinitionBean(GenerationType.NESTED, count, relationData, fieldData);
    }

    private RelationDefinitionBean(GenerationType generationType,
                                   int count,
                                   Map<String, RelationDefinitionBean> relationData,
                                   Map<String, Object> fieldData) {
        this.generationType = generationType;
        this.count = count;
        this.relationData = relationData;
        this.fieldData = fieldData;
    }

    public GenerationType getGenerationType() {
        return generationType;
    }

    public int getCount() {
        return count;
    }

    public Map<String, RelationDefinitionBean> getRelationData() {
        return relationData;
    }

    public Map<String, Object> getFieldData() {
        return fieldData;
    }

    public enum GenerationType {
        FIRST, RANDOM, NESTED
    }
}
