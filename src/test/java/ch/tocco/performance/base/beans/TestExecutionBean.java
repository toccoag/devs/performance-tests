package ch.tocco.performance.base.beans;

public class TestExecutionBean {

    private final String test;
    private final int buildId;
    private final int warmupIterations;
    private final int measurementIterations;
    private final double mean;
    private final int max;
    private final int min;
    private final String raw;

    public TestExecutionBean(String test,
                             int buildId,
                             int warmupIterations,
                             int measurementIterations,
                             double mean,
                             int max,
                             int min,
                             String raw) {
        this.test = test;
        this.buildId = buildId;
        this.warmupIterations = warmupIterations;
        this.measurementIterations = measurementIterations;
        this.mean = mean;
        this.max = max;
        this.min = min;
        this.raw = raw;
    }

    public String getTest() {
        return test;
    }

    public int getBuildId() {
        return buildId;
    }

    public int getWarmupIterations() {
        return warmupIterations;
    }

    public int getMeasurementIterations() {
        return measurementIterations;
    }

    public double getMean() {
        return mean;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    public String getRaw() {
        return raw;
    }
}
