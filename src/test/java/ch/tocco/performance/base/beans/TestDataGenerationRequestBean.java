package ch.tocco.performance.base.beans;

import java.util.Map;

public class TestDataGenerationRequestBean {

    private final String entityModel;
    private final int count;
    private final Map<String, RelationDefinitionBean> relationData;
    private final Map<String, Object> fieldData;

    public TestDataGenerationRequestBean(String entityModel,
                                         int count,
                                         Map<String, RelationDefinitionBean> relationData,
                                         Map<String, Object> fieldData) {
        this.entityModel = entityModel;
        this.count = count;
        this.relationData = relationData;
        this.fieldData = fieldData;
    }

    public String getEntityModel() {
        return entityModel;
    }

    public int getCount() {
        return count;
    }

    public Map<String, RelationDefinitionBean> getRelationData() {
        return relationData;
    }

    public Map<String, Object> getFieldData() {
        return fieldData;
    }
}
