package ch.tocco.performance.base.beans;

import java.util.Map;

public class EntityBean {

    private String model;
    private String key;
    private String version;

    private Map<String, EntityPathBean> paths;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Map<String, EntityPathBean> getPaths() {
        return paths;
    }

    public void setPaths(Map<String, EntityPathBean> paths) {
        this.paths = paths;
    }
}
