package ch.tocco.performance.base.beans;

import java.util.List;

public class PrimaryKeyListBean {

    private List<String> values;

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
