package ch.tocco.performance.base.managers;

import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;

public interface ResultManager {

    void process(UUID requestId, String name, WebClient webClient) throws Exception;

    void testFailed(String name);
}
