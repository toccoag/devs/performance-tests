package ch.tocco.performance.base.managers;

import ch.tocco.performance.base.beans.CountBean;
import ch.tocco.performance.base.beans.EntitiesBean;
import ch.tocco.performance.base.beans.TestExecutionBean;
import ch.tocco.performance.base.beans.TestExecutionEvaluationBean;
import ch.tocco.performance.base.connectors.DatabaseConnector;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

@Component
@Profile("!dev")
public class ProductionResultManager implements ResultManager {

    @Value("${performance.buildId}")
    private int buildId;

    @Value("${performance.revision}")
    private String revision;

    @Value("${performance.test.warmupIterations}")
    protected int warmupIterations;

    @Value("${performance.test.measurementIterations}")
    protected int measurementIterations;

    @Value("${performance.result.retries}")
    private int retries;

    @Autowired
    private DatabaseConnector databaseConnector;

    @Autowired
    private PlotGenerator plotGenerator;

    @Autowired
    private ReportManager reportManager;

    @Override
    public void process(UUID requestId, String name, WebClient webClient) throws Exception {
        waitUntilResultsAreAvailable(requestId, webClient);
        List<Integer> measurements = collectResults(requestId, webClient);
        saveResults(name, measurements);
        analyzeResults(name);
    }

    @Override
    public void testFailed(String name) {
        reportManager.testFailed(name);
    }

    private void waitUntilResultsAreAvailable(UUID requestId, WebClient webClient) throws Exception {
        long resultCount = 0;
        int attempts = 0;

        while (resultCount < measurementIterations) {
            CountBean countBean = webClient.get()
                    .uri(builder -> builder.path("/entities/2.0/System_activity/count")
                            .queryParam("_where", "request_id == \"" + requestId.toString() + "\"").build())
                    .retrieve()
                    .bodyToMono(CountBean.class)
                    .block();

            assertNotNull(countBean);
            resultCount = countBean.getCount();

            if (resultCount < measurementIterations) {
                if (++attempts >= retries) {
                    fail("Expected " + measurementIterations + " measurements, but got only " + resultCount);
                }
                Thread.sleep(1000);
            }
        }
    }

    private List<Integer> collectResults(UUID requestId, WebClient webClient) {
        EntitiesBean entitiesBean = webClient.get()
                .uri(builder ->
                        builder.path("/entities/2.0/System_activity")
                                .queryParam("_paths", "execution_time")
                                .queryParam("_where", "request_id == \"" + requestId.toString() + "\"")
                                .queryParam("_sort", "timestamp")
                                .build()
                )
                .retrieve()
                .bodyToMono(EntitiesBean.class)
                .block();
        assertNotNull(entitiesBean);

        return entitiesBean.getData()
                .stream()
                .map(entityBean -> (int) entityBean.getPaths().get("execution_time").getValue())
                .collect(Collectors.toList());
    }

    private void saveResults(String name, List<Integer> measurements) {
        double mean = measurements.stream()
                .mapToDouble(m -> m)
                .average()
                .orElse(0);
        int max = measurements.stream()
                .mapToInt(m -> m)
                .max()
                .orElse(0);
        int min = measurements.stream()
                .mapToInt(m -> m)
                .min()
                .orElse(0);

        TestExecutionBean testExecution = new TestExecutionBean(name, buildId, warmupIterations,
                measurementIterations, mean, max, min, new Gson().toJson(measurements));
        databaseConnector.createExecution(buildId, revision);
        databaseConnector.createTestExecution(testExecution);
    }

    private void analyzeResults(String name) throws IOException {
        List<TestExecutionEvaluationBean> executions = databaseConnector.getTestExecutions(name, 25);
        plotGenerator.plot(name, executions);

        TestExecutionEvaluationBean currentExecution = executions.get(executions.size() - 1);
        double differencePrevious = 0;
        double differenceFastest = 0;
        double differenceSlowest = 0;

        if (executions.size() > 1) {
            TestExecutionEvaluationBean previousExecution = executions.get(executions.size() - 2);

            List<Double> differences = executions.subList(0, executions.size() - 1)
                    .stream()
                    .mapToDouble(TestExecutionEvaluationBean::getMean)
                    .map(mean -> getPercentageDifference(mean, currentExecution.getMean()))
                    .boxed()
                    .collect(Collectors.toList());

            differencePrevious = getPercentageDifference(previousExecution.getMean(), currentExecution.getMean());
            differenceFastest = Collections.max(differences);
            differenceSlowest = Collections.min(differences);
        }

        reportManager.addTest(name, currentExecution.getMin(), currentExecution.getMean(), currentExecution.getMax(),
                differencePrevious, differenceFastest, differenceSlowest);
    }

    private double getPercentageDifference(double prevMean, double currentMean) {
        return 100 * (1 - prevMean / currentMean);
    }
}
