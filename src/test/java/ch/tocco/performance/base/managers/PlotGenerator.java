package ch.tocco.performance.base.managers;

import ch.tocco.performance.base.beans.TestExecutionEvaluationBean;
import org.knowm.xchart.*;
import org.knowm.xchart.style.XYStyler;
import org.knowm.xchart.style.markers.SeriesMarkers;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PlotGenerator {
    private final Color WHITE = new Color(255, 255, 255, 255);
    private final Color LIGHT_GREY = new Color(0, 0, 0, 150);
    private final Color BLACK = new Color(0, 0, 0, 180);
    private final int WIDTH = 1600;
    private final int HEIGHT = 800;

    public void plot(String name, List<TestExecutionEvaluationBean> executions) throws IOException {
        XYChart chart = new XYChartBuilder().width(WIDTH).height(HEIGHT).title(name).build();
        setLayout(chart);
        setXTicks(chart, executions);
        draw(chart, executions);
        save(name, chart);
    }

    private void setLayout(XYChart chart) {
        XYStyler styler = chart.getStyler();
        styler.setLegendVisible(false);
        styler.setAxisTitlesVisible(false);
        styler.setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Area);
        styler.setYAxisMin(0.0);
        styler.setXAxisLabelRotation(90);
        styler.setPlotGridHorizontalLinesVisible(false);
        styler.setPlotGridVerticalLinesVisible(false);
    }

    private void setXTicks(XYChart chart, List<TestExecutionEvaluationBean> executions) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
        Map<Object, Object> map = executions.stream().collect(Collectors.toMap(TestExecutionEvaluationBean::getBuildId,
                e -> e.getBuildId() + " / " + dateFormat.format(e.getCreated())));
        chart.setCustomXAxisTickLabelsMap(map);
    }

    private void draw(XYChart chart, List<TestExecutionEvaluationBean> executions) {
        double[] xData = executions.stream().map(TestExecutionEvaluationBean::getBuildId).mapToDouble(Integer::intValue).toArray();
        double[] maxData = executions.stream().map(TestExecutionEvaluationBean::getMax).mapToDouble(Integer::intValue).toArray();
        double[] meanData = executions.stream().map(TestExecutionEvaluationBean::getMean).mapToDouble(Double::doubleValue).toArray();
        double[] minData = executions.stream().map(TestExecutionEvaluationBean::getMin).mapToDouble(Integer::intValue).toArray();

        XYSeries max = chart.addSeries("max", xData, maxData);
        max.setLineColor(WHITE);
        max.setFillColor(LIGHT_GREY);
        max.setMarker(SeriesMarkers.NONE);
        max.setXYSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Area);

        XYSeries mean = chart.addSeries("mean", xData, meanData);
        mean.setLineColor(BLACK);
        mean.setMarker(SeriesMarkers.NONE);
        mean.setXYSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);

        XYSeries min = chart.addSeries("min", xData, minData);
        min.setLineColor(WHITE);
        min.setFillColor(WHITE);
        min.setMarker(SeriesMarkers.NONE);
        min.setXYSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Area);
    }

    private void save(String name, XYChart chart) throws IOException {
        File outputDirectory = new File("./build/performance/plots");
        assert outputDirectory.exists() || outputDirectory.mkdirs();
        File file = new File(outputDirectory, name + ".png");
        BitmapEncoder.saveBitmap(chart, new FileOutputStream(file), BitmapEncoder.BitmapFormat.PNG);
    }
}
