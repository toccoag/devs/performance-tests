package ch.tocco.performance.base.managers;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;

@Component
@Profile("dev")
public class DevResultManager implements ResultManager {

    @Override
    public void process(UUID requestId, String name, WebClient webClient) {

    }

    @Override
    public void testFailed(String name) {

    }
}
