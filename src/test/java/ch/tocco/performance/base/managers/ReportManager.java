package ch.tocco.performance.base.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

@Component
public class ReportManager {
    private final Logger logger = LoggerFactory.getLogger(ReportManager.class);

    private final String CSV_HEADER = "name;execution_failed;min;mean;max;difference_previous;difference_fastest;difference_slowest\n";
    private File report;

    public ReportManager() {
        try {
            File outputDirectory = new File("./build/performance");
            assert outputDirectory.exists() || outputDirectory.mkdirs();
            this.report = new File(outputDirectory, "report.csv");
            Files.write(Path.of(report.toURI()), CSV_HEADER.getBytes());
        } catch (IOException e) {
            logger.error("Unable to write the header to the report file");
            System.exit(1);
        }
    }

    public void addTest(String name,
                        int min,
                        double mean,
                        int max,
                        double differencePrevious,
                        double differenceFastest,
                        double differenceSlowest) {
        writeLine(String.format("%s;%s;%d;%.2f;%d;%.2f;%.2f;%.2f\n", name, false, min, mean, max, differencePrevious,
                differenceFastest, differenceSlowest));
    }

    public void testFailed(String name) {
        writeLine(String.format("%s;%s;;;;;;\n", name, true));
    }

    private void writeLine(String line) {
        try {
            Files.write(Path.of(report.toURI()), line.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            logger.error("Unable to write a line to the report file");
            System.exit(1);
        }
    }
}
