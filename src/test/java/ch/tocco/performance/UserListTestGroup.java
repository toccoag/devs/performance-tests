package ch.tocco.performance;

import java.util.Map;

import org.junit.jupiter.api.Nested;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import ch.tocco.performance.base.AbstractPerformanceTestGroup;

import static ch.tocco.performance.base.beans.RelationDefinitionBean.*;
import static org.junit.jupiter.api.Assertions.*;

public class UserListTestGroup extends AbstractPerformanceTestGroup {

    @Override
    protected void configureTestInstance(WebClient webClient) {
        int eventCount = 10;
        int userCount = 10 * 100;

        insertTestData(webClient, "Event", eventCount, Map.of(), Map.of("participation_max", userCount));
        insertTestData(webClient, "User", userCount, Map.of(
            "relRegistration", nested(Map.of(
                "relEvent", random(eventCount)
            )),
            "relAddress_user", nested(Map.of(
                "relAddress", nested(Map.of(
                    "relCountry_b", anyEntity(),
                    "relCountry_d", anyEntity()
                ))
            ))
        ));
    }

    @Nested
    class ReadEntitiesWithPaths extends AbstractPerformanceTestCase {

        @Override
        protected void executeTest(WebClient webClient) {
            ClientResponse response = webClient.get()
                .uri(builder -> builder.path("/entities/2.0/User")
                    .queryParam("_limit", "100")
                    .queryParam("_paths", "firstname,lastname,relAddress_user(relAddress(address_b,zip_b,city_b)),relRegistration.relEvent.label,relUser_status.unique_id")
                    .build())
                .exchange()
                .block();

            assertNotNull(response);
            assertTrue(response.statusCode().is2xxSuccessful());
        }
    }

    @Nested
    class ExportUsers extends AbstractPerformanceTestCase {
        @Override
        protected void executeTest(WebClient webClient) {
            ClientResponse response = webClient.get()
                    .uri(builder -> builder.path("/performance/export/User").build())
                    .exchange()
                    .block();

            assertNotNull(response);
            assertTrue(response.statusCode().is2xxSuccessful());
        }
    }
}
