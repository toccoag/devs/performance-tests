package ch.tocco.performance;

import ch.tocco.performance.base.AbstractPerformanceTestGroup;
import ch.tocco.performance.base.beans.RelationDefinitionBean;
import ch.tocco.performance.base.beans.TestDataGenerationRequestBean;
import ch.tocco.performance.base.beans.report.GeneralSettingsBean;
import ch.tocco.performance.base.beans.report.QueryBean;
import ch.tocco.performance.base.beans.report.ReportGenerationBean;
import ch.tocco.performance.base.beans.report.SelectionBean;
import org.junit.jupiter.api.Nested;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

import static ch.tocco.performance.base.beans.RelationDefinitionBean.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReportTestGroup extends AbstractPerformanceTestGroup {

    protected int getWarmupIterations() {
        return 1;
    }

    protected int getMeasurementIterations() {
        return 2;
    }

    @Override
    protected void configureTestInstance(WebClient webClient) {
        int userCount = 1000;
        int caseCount = 100;

        insertTestData(webClient, "User", userCount, Map.of(
            "relAddress_user", nested(Map.of(
                "relAddress", nested(Map.of(
                    "relCountry_b", anyEntity(),
                    "relCountry_d", anyEntity()
                ))
            ))));

        insertTestData(webClient, "Case", caseCount, Map.of(
            "relUser", random(userCount),
            "relAdviser", random(userCount),
            "relSubvention_canton", random(26),
            "relCase_type", random(6),
            "relCase_status", random(6)));
    }

    @Nested
    class CreateListReport extends AbstractPerformanceTestCase {
        @Override
        protected void executeTest(WebClient webClient) {
            executeReportTest(webClient, "phonelist", "User", "phonelist.pdf", "true");
        }
    }

    @Nested
    class CreatePersonalReport extends AbstractPerformanceTestCase {
        @Override
        protected void executeTest(WebClient webClient) {
            executeReportTest(webClient, "personnel_sheet", "Case", "personnel_sheet.pdf", "true");
        }
    }

    private void executeReportTest(WebClient webClient, String reportId, String entityModel, String filename, String query) {
        ReportGenerationBean generationBean = getGenerationBean(entityModel, query, "de", filename, "tocco_standard", "archived");

        ClientResponse response = webClient.post()
            .uri(builder -> builder.path("/report/{id}/generations").build(reportId))
            .header("X-Business-Unit", "test1")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(generationBean)
            .exchange()
            .block();

        assertNotNull(response);
        assertTrue(response.statusCode().is2xxSuccessful());
    }


    private ReportGenerationBean getGenerationBean(String entityModel,
                                                   String query,
                                                   String language,
                                                   String filename,
                                                   String corporateDesign,
                                                   String archiveType) {

        QueryBean queryBean = new QueryBean();
        queryBean.setWhere(query);

        SelectionBean selectionBean = new SelectionBean();
        selectionBean.setEntityName(entityModel);
        selectionBean.setQuery(queryBean);
        selectionBean.setType("QUERY");

        GeneralSettingsBean settingsBean = new GeneralSettingsBean();
        settingsBean.setLanguage(language);
        settingsBean.setFilename(filename);
        settingsBean.setCorporateDesign(corporateDesign);
        settingsBean.setArchiveType(archiveType);

        ReportGenerationBean generationBean = new ReportGenerationBean();
        generationBean.setEntityModel(entityModel);
        generationBean.setSelection(selectionBean);
        generationBean.setGeneralSettings(settingsBean);

        return generationBean;
    }
}
