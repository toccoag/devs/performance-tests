package ch.tocco.performance;

import ch.tocco.performance.base.AbstractPerformanceTestGroup;
import org.apache.pdfbox.io.IOUtils;
import org.junit.jupiter.api.Nested;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ImporterTestGroup extends AbstractPerformanceTestGroup {
    @Override
    protected int getWarmupIterations() {
        return 1;
    }

    @Override
    protected int getMeasurementIterations() {
        return 1;
    }

    @Nested
    class ImportExcel extends AbstractPerformanceTestCase {
        @Override
        protected void executeWarmup(WebClient webClient) throws IOException {
            executeImporterTest(webClient, "importer_warmup.xlsx");
        }

        @Override
        protected void executeTest(WebClient webClient) throws IOException {
            executeImporterTest(webClient, "importer_test.xlsx");
        }

        private void executeImporterTest(WebClient webClient, String filename) throws IOException {
            try (InputStream excelFile = this.getClass().getResource(filename).openStream()) {
                MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
                bodyBuilder.part("file", IOUtils.toByteArray(excelFile)).filename("importer_test.xlsx");

                ClientResponse response = webClient.post()
                        .uri(builder -> builder.path("/performance/import/User").build())
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .bodyValue(bodyBuilder.build())
                        .exchange()
                        .block();

                assertNotNull(response);
                assertTrue(response.statusCode().is2xxSuccessful());
            }
        }
    }
}
