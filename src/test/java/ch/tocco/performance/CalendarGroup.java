package ch.tocco.performance;

import ch.tocco.performance.base.AbstractPerformanceTestGroup;
import ch.tocco.performance.base.beans.EntitiesBean;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Nested;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ch.tocco.performance.base.beans.RelationDefinitionBean.*;
import static ch.tocco.performance.base.beans.RelationDefinitionBean.nested;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalendarGroup extends AbstractPerformanceTestGroup {

    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private List<String> userKeys;
    private List<String> roomKeys;
    private List<String> eventKeys;

    @Override
    protected void configureTestInstance(WebClient webClient) {
        int userCount = 30;
        int roomCount = 5;
        int numberOfReservationsPerEvent = 10;

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        startDate = LocalDateTime.now().atZone(ZoneId.systemDefault());
        endDate = startDate.plusDays(30);

        insertTestData(webClient, "Building", 1, Map.of());
        roomKeys = insertTestData(webClient, "Room", roomCount, Map.of(
                "relBuilding", anyEntity()
        ));
        userKeys = insertTestData(webClient, "User", userCount, Map.of(
                "relRegistration", nested(Map.of(
                        "relEvent", nested(
                                Map.of(
                                        "relReservation", nestedMany(numberOfReservationsPerEvent,
                                                Map.of("relRoom", random(roomCount)),
                                                Map.of("date_from", startDate.format(dateFormatter), "date_till", endDate.format(dateFormatter))
                                        )
                                ), Map.of("participation_max", Integer.MAX_VALUE)
                        )
                ))
        ));
        loadEventKeys(webClient);
    }

    private void loadEventKeys(WebClient webClient) {
        EntitiesBean entitiesBean = webClient.get()
                .uri(builder ->
                        builder.path("/entities/2.0/Event")
                                .queryParam("_paths", "pk")
                                .build()
                )
                .retrieve()
                .bodyToMono(EntitiesBean.class)
                .block();
        assertNotNull(entitiesBean);

        eventKeys = entitiesBean.getData()
                .stream()
                .map(entityBean -> Integer.toString((Integer) entityBean.getPaths().get("pk").getValue()))
                .collect(Collectors.toList());
    }

    @Nested
    class ReadCalendarEvents extends AbstractPerformanceTestCase {
        @Override
        protected void executeTest(WebClient webClient) {
            ClientResponse response = webClient.post()
                    .uri(builder -> builder.path("/calendar/events").build())
                    .bodyValue(new CalendarEventsRequest(userKeys, roomKeys, eventKeys, startDate, endDate))
                    .exchange()
                    .block();

            assertNotNull(response);
            assertTrue(response.statusCode().is2xxSuccessful());
        }
    }

    private static class CalendarEventsRequest {
        private final List<CalendarBean> calendars = Lists.newArrayList();
        private final long start;
        private final long end;

        public CalendarEventsRequest(List<String> users, List<String> rooms, List<String> events, ZonedDateTime start, ZonedDateTime end) {
            this.start = start.toInstant().toEpochMilli();
            this.end = end.toInstant().toEpochMilli();
            calendars.add(new CalendarBean("participant", users));
            calendars.add(new CalendarBean("room", rooms));
            calendars.add(new CalendarBean("event", events));
        }

        public List<CalendarBean> getCalendars() {
            return calendars;
        }

        public long getStart() {
            return start;
        }

        public long getEnd() {
            return end;
        }
    }

    private static class CalendarBean {
        private final String calendarTypeId;
        private final List<String> keys;

        public CalendarBean(String calendarTypeId, List<String> keys) {
            this.calendarTypeId = calendarTypeId;
            this.keys = keys;
        }

        public String getCalendarTypeId() {
            return calendarTypeId;
        }

        public List<String> getKeys() {
            return keys;
        }
    }
}
