CREATE TABLE execution (
    build_id INTEGER UNIQUE NOT NULL,
    revision VARCHAR(40) NOT NULL,
    created TIMESTAMP,
    PRIMARY KEY(build_id)
);

CREATE TABLE test_execution (
    id SERIAL PRIMARY KEY,
    test VARCHAR(255) NOT NULL,
    build_id INTEGER NOT NULL,
    warmup_iterations INTEGER NOT NULL,
    measurement_iterations INTEGER NOT NULL,
    mean NUMERIC NOT NULL,
    max INTEGER NOT NULL,
    min INTEGER NOT NULL,
    raw TEXT NOT NULL,
    CONSTRAINT fk_execution FOREIGN KEY(build_id) REFERENCES execution(build_id),
    UNIQUE (build_id, test)
);