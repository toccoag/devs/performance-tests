# Performance Testing

## Run Tests

**Build:**
```
./gradlew build
```

There are two options to execute tests. In the first one the test is executed but the environment is not "resetted".
Normally you can choose this simplified option. In the second option the full pipeline (reset database, 
restart openshift container, draw plots etc.) of a test execution is performed as in production.

### Simplified Execution

**Add a local config file:**

Config file location: `src/main/resources/application-dev.properties`:

Required properties in file:
```
spring.main.allow-bean-definition-overriding=true

spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration

performance.test.baseUrl=http://localhost:8080/nice2/rest
performance.test.user=******
performance.test.apiKey=******
```

**Important: The database is not resetted after an execution of a test group. The developer must manually ensure that the
database is in a valid state. As an example an event has a maximum number of participants. If a new registration is created
with a relation to an event where the maximum number of participants is already reached the test will fail.**

**Run:**

Run a test with VM option `-Dspring.profiles.active=dev`

### Full Pipeline Execution

* Create a database `performance_testing` with the schema `src/main/resources/database.sql`
* Copy the file `src/main/resources/application.properties` to `src/main/resources/application.local.properties`
* Set the empty application properties
    * Set the database url, user and password
    * `performance.buildId` build id of teamcity
     and `performance.revision` git commit id
    * `performance.startup.retries` number of retries (1 retry per second) the tool is waiting that rest api is ready after the restart of pod
    * `performance.result.retries` number of retries (1 retry per second) the tool is waiting that the system activity entities are created
    * `performance.database.database` is the database which the nice instance is using
    * `performance.database.databaseTemplate` is the database template which is restored after each test group execution
    * `performance.test.baseUrl`, `performance.test.user` and `performance.test.apiKey` are used to access the nice rest api
    * `performance.openshift.url`, `performance.openshift.oauth` and `performance.openshift.project` are used to restart an openshift container
