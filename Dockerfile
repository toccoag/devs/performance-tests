FROM gradle:6.6-jdk11-openj9
ENV GRADLE_USER_HOME=/repo/.gradle-cache

COPY . /repo
WORKDIR /repo
RUN gradle testClasses

CMD [ "gradle", "test" ]
